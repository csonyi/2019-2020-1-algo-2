# A Kahootról

* Az órai kvízek a "Kahoot!" nevű platform segítségével lesznek megrendezve
* A játék felülete elérhető a "kahoot.it" weblap megnyitásával (laptop, telefon, bármi) vagy a "Kahoot!" (lila alapon fehér "K!" felirat az ikonja) okostelefonos applikáció "Enter PIN" menüpontjának kiválasztásával
* Fel fogok írni a táblára egy számot, ezt a számot beírva, majd utána egy nicknevet megadva (ami a neved vagy a neptun-kódod legyen, hogy utólag tudjam, kinek mennyi pont jár) lehet csatlakozni a játékhoz
* A játék elindítása után 3-4 kérdést fogunk lejátszani az alábbi módon:
    * Előbb felteszem a kérdést szóban (ez lehet egy eldöntendő kérdés, egy mondat kiegészítése a válaszlehetőségekkel, de bármi más, amire 2-4 válaszlehetőség adható)
    * Majd megjelenik előttetek 2, 3 vagy 4 gomb, balról jobbra, illetve fentről lefelé az alábbi sorrendben:
        * Piros háromszög
        * Kék rombusz
        * Sárga kör
        * Zöld négyzet
    * Amíg ezeket látjátok, elmondom a 2, 3 vagy 4 válaszlehetőség szövegét a fenti sorrendben
    * A jót meg kell jelölni
    * Erre a színes gombok megjelenésétől kezdve (ha mást nem mondok) 30 mp áll rendelkezésre
    * Előfordulhat, hogy több helyes megoldás is van, ezt külön nem feltétlen fogom bejelenteni. Ekkor bármelyik helyes megoldás kiválasztása maximális pontot ér
* A kérdések között látható lesz majd a helyes megoldás és a játék állása
* A pontozás a helyességet és a gyorsaságot veszi alapul
* A kvíz a következő célokat szolgálja:
    * Díjazni a jelenlétet és az óráról órára készülést
    * Segíteni a tudás elmélyítését bizonyos érdekesebb kérdésekkel - a játék után át is beszéljük a helyes megoldásokat!
    * Megtörni a monotonitást