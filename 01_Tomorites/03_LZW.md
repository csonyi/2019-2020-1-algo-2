# Lempel-Ziv-Welch-kódolás

* Itt most nem karakterenként, hanem bizonyos részstringenként fogunk kódolni
* A kódolt egységek kódhossza itt most fix (bár működhet az algoritmus változó kódhosszal is)
* Az input egyszeri (pufferelt) végigolvasása elég lehet a kódoláshoz. Ha nem ismert, akkor előbb ki kell gyűjteni az ábécét, tehát akkor kétszer kell az inputon végigmenni
* A dekódolásoz elég csak az ábécét mellékelni, az algoritmus mind a kódolásnál, mind a dekódolásnál egy gyorsan, oda-vissza kereshető szótárt épít

## Kódolás - Példa lejátszás

* Legyen az input az **ababcababa**
* Az ábécé értelemszerűen az {a,b,c}
* A kódtáblát (szótárat) előbb az ábécé elemeivel inicializáljuk, azaz rendre minden betűhöz hozzárendeljük annak sorszámát:

```
1   a
2   b
3   c
```

* Most a következő lépéseket ismételgetjük amíg el nem fogy az input:
    * Olvasunk addig, amíg az addig beolvasott string még bennvan a szótárban
    * Ennek a stringnek a kódját kiírjuk az outputra
    * Majd az eggyel hosszabb stringet (tehát az épp kiírtat, plusz az azt követő karaktert, ami így már nem volt benne a szótárban) felvesszük a szótárba, a következő kiosztható kóddal
* Ebből az következik, hogy jó eséllyel kevesebb karakterből fog állni az output, mint az input, de cserébe az (output) ábécé mérete nőni fog
* Viszont csak az eredeti ábécét kell elküldeni, a kódolt string ábécéjét dekódolás közben ki tudjuk újra számolni; tehát bár a számolás "tárigényes", de abból elküldeni bőven nem kell mindent
* Akkor lesz jó a kódolási arány, ha sok ismétlődő - akár hosszabb - mintát tartalmaz az egy viszonylag kisebb ábécé felett értelmezett input string
* Példánkban, az első karakter olvasásakor - értelemszerűen mivel az ábécé ezeket tartalmazza - fogunk találni annak megfelelő kódot, viszont az első két karakter által alkotott részstringre már nem. Ezért leírjuk annak a kódját, majd felvesszük ezt a kétbetűs stringet:

```
1   a
2   b
3   c
4   ab

in: <a|babcababa>
out: 1
```

* Mivel az első karakterig írtuk ki eddig a kódolt verziót, most a második karakterrel fogjuk a feldolgozást folytatni. Mivel pufferelve olvasunk, igazából mindig, amikor "szemantikailag" az x. karakternél tartunk, technikailag már kiolvastuk az x+1. karaktert is. Erre szükségünk is van, mert mindig fel kell vennünk a szótárba azt a stringet, ami az első olyan volt, ami még nem volt benne
* Lépésről lépésre leírom a továbbiakat is:

```
1   a
2   b
3   c
4   ab
5   ba

in: <ab|abcababa>
out: 1,2
```

* Itt látszik effektíve egy tömörítési lépés; a gyakran ismétlődő "ab" szakasznak immáron dedikált kódja van. Persze visszamenőlegesen nem írjuk át az első "ab"-t erre, mert nem lehetne csak az ábécé segítségével dekódolni, illetve az algoritmus futása se lenne ennyire hatékony

```
1   a
2   b
3   c
4   ab
5   ba
6   abc

in: <abab|cababa>
out: 1,2,4
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   ca

in: <ababc|ababa>
out: 1,2,4,3
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   ca
8   aba

in: <ababcab|aba>
out: 1,2,4,3,4
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   ca
8   aba

in: <ababcababa|>
out: 1,2,4,3,4,8
```

* Amikor befejeződik a string, kiírjuk az éppen épülőben levő kódot, a szótárba már nem kell (nem is tudunk) felvenni semmit
* Mennyi a példában a kódolási hatékonyság?
    * Eredetileg 10 karakteres volt az input, ami egy byte-os karakterekkel számolva 10 B
    * Az output 6 karakter hosszú egy olyan ábécé felett, aminek 8 eleme van
    * Emiatt minden elemet x biten kódolhatunk, ahol 2<sup>x</sup> nagyobb, vagy egyenlő, mint 8
    * Minden kódolt karaktert tehát 3 biten tárolunk, emiatt 6*3 = 18 bites a kódolt kód, plusz még át kell küldeni az ábécét is

## Kódolás - Algoritmus

```
lzwCode(in : InStream) : OutStream
    out := <>
    stringTable := initFromAlphabet()
    c := in.read()
    prev := eps
    current := c
    AMÍG c != EOF
        HA current eleme stringTable
            c := in.read()
            prev := current
            current := prev + c
        KÜLÖNBEN
            out.print(stringTable.get(prev))
            stringTable.add(current)
            current := c
    out.print(stringTable.get(prev))
    return out
```

* Az input egy bemeneti folyam, ezen egy read() művelet adott, amiből mindig a soron következő karaktert tudjuk kiolvasni, a kiolvasás után az a karakter örökre elveszik az inputról
* Az InStream kiürülését úgy tudjuk megfogni, hogy ellenőrizzük, a legutolsó karakter az EOF (end-of-file) karakter-e
* A kimenet egy kimeneti folyam, ami az elején inicializálunk üres sorozatra - úgymond töröljük a konzolt, írni pedig a print() metódussal tudunk rá karakterenként
* A folyamokban nincs lehetőség előre-hátra ugrani, azaz nincs random access
* Tételezzük fel, hogy van egy olcsó initFromAlphabet() nevű függvényünk, ami a string tábla alaphelyzetét előállítja, a fenti példában az a-1, b-2, c-3 hozzárendeléseket
* A c típusa character, ő jelenti majd a puffert
* A prev típusa string, kezdetben "eps"-szel, azaz ε-nal (epszilonnal) inicializáljuk. Jelentse ez az üres stringet
* Az invariáns (azaz a függvény futása során mindvégig igaz állítás) a prevre az, hogy ez az a leghosszabb string, ami még biztos benne van a szótárban
* A current típusa string, itt kicsit csalunk, mivel egy charactert teszünk vele egyenlővé, ezt most explicit kasztolás nélkül fogadjuk el, ez úgyis implementációfüggő
* A current eleme stringTable lekérdezés azt mondja meg, van-e olyan érték a táblázatban (tehát annak második oszlopában), ami egyenlő currenttel
* A stringTable.get(prev) lekéri prev kódját
* A stringtable.add(current) pedig hozzáadja a current string értéket a következő elérhető int kulccsal (ennek kiszámítását a stringTable-re bízzuk)
* A "+" operátor esetünkben a konkatenációt jelenti, egy stringből és egy karakterből előállítja az azok összefűzéséből keletkező stringet
* Tehát mielőtt a ciklusba érünk, c-ben az első karakter van, prevben az üres szó, currentben pedig a prev + az első karakter. Egy újabb invariáns, hogy currentben a már biztosan adatbázisban levő szó (azaz prev) + a következő karakter összefűzése van
* A ciklusban addig megyünk, amíg sikerült mit olvasnunk
* Ha az derül ki, hogy current - aminek a prefixeiről már eddig is tudtuk - is a szótár része, akkor prevet egyenlővé tesszük currenttel (hisz annak invariánsa már igaz rá), current mögé pedig bátran beolvassuk a következő karaktert
* Ellenben, ha current már nem tagja a táblának, fel kell vennünk oda, egyúttal a legutóbbi még táblabeli elem kódját kiírhatjuk. Ekkor a fenti példában a "vonal mögül" kezdjük újra a current kódszó építését, ami a pufferként beolvasott c-től való újrakezdésként értelmezhető
* Az egész legvégén azt tudjuk, hogy c-ben az EOF van, tehát nem kell felvenni újabb elemet, viszont prevben ott van az a leghosszabb string, ami még az EOF előtt épült, de anélkül, hogy azt kiírtuk volna! A végén ki kell tehát írnunk prev kódját
    * Talán nem is olyan egyértelmű itt a kód értelmezése. Ha EOF olvasása úgy talál minket, hogy az előző körben current a stringtábla eleme volt, akkor világos a helyzet: beolvastuk EOF-ot, azt hozzáadtuk currenthez (bármi is legyen ennek a szemantikája), és az előző current (ami tehát valid volt) bekerült prevbe. Utána kilépünk, prevben kiírandó string van
    * Ha az előző karakterre nem volt benne a táblában, akkor pedig kiírjuk azt a prevet, felvesszük azt a currentet és c-be beírjuk az illeszkedést elrontó (utolsó nem-EOF) karaktert. De itt nincs újabb olvasás, tehát megyünk egy újabb kört. Mivel ez egy karakter, ezért biztosan az ábécé része, biztosan benne van a stringtáblában. Ezért a bal ágra fogunk esni, azt az esetet pedig az előbb már megvizsgáltuk
    * Ha elfogadjuk azt, hogy current + EOF = current + = current, akkor végülis itt prev helyett current is írható lehet; de ez nem feltétel

## Alternatív szótárépítés

* Az algoritmust olyan verzióban is elkészíthetjük, hogy az épülő szótárt is eleve tömörítse. Egész pontosan minden inicializálásból adódó bejegyzésnél (az ábécé betűi) a kódolt egység egy karakter hosszú, a többi bejegyzésnél két karakter hosszú lesz (egy rekurzív hivatkozás a prefix kódjára, majd az utolsó karakter)
* A kódolás mechanizmusa ugyanaz, a szótárba mentés módosul kissé
* A fenti példa inputjára ez a szótár keletkezne ezzel a módszerrel:

```
1   a
2   b
3   c
4   1b
5   2a
6   4c
7   3a
8   4a
```

## Dekódolás - Elv és lejátszás

* Ha a készített szótárt is átadjuk, a dekódolás pofonegyszerű, csak a számok szerint vissza kell helyettesíteni a map értékeit
    * Persze itt (és a bonyolultabb esetekben is) életbevágó, hogy az a map/dictionary/asszoiatív tömb, amivel reprezentáljuk a szótárt, oda-vissza kereshető legyen. Hisz míg a kódolásnál az értékek szerint kerestük a kulcsot, most a kulcs szerint keressük az értéket
* Ha a fenti, alternatív szótárt küldjük el - aminek a mérete már kisebb, mint az eredetié - egy rekurzív algoritmust kell írnunk, ami tehát minden kódindexre behelyettesíti annak értékét, majd abból újra behelyettesíti az annak első felét jelentő másik számot, egész addig, amíg ilyen van
* De a legérdekesebb eset az, amikor nem küldünk szótárat, csak az ábécét (az eredeti sorrendben!). Ez nyilvánvalóan a legkisebb átküldendő üzenetet, azaz a leghatékonyabb tömörítést jelenti, de a dekódolás műveletigényén se dob nagyságrendeket, ugyanúgy elég lesz egyszer végigmenni az inputon
* Lássuk példaképp a fentit. Adott tehát a kódolt verzió és az ábécé, amiből megint csak inicializáljuk a táblát:

```
1   a
2   b
3   c

in: 1,2,4,3,4,8
out: <>
```

* Vegyük az első elemet. Ez az 1-es. Ennek benn kellett lennie a táblában, hiszen ez volt valaha a leghosszabb olyan prefix, ami még benne volt a táblában. Láthatjuk, az "a" kódja ez, ezt tehát leírhatjuk
* Azt még tudjuk, hogy amikor ezt az egyest leírtuk a kódolás során, eggyel tovább mentünk és az "ax" stringet, ahol x a következő betű, felvettük 4-es kóddal

```
1   a
2   b
3   c
4   ax

in: 1|2,4,3,4,8
out: <a>
```

* Most a kettesnél tartunk, aminek az értéke "b", és tudjuk, hogy valami "by"-t is felvettünk. Azt is tudjuk, hogy az előző után a "b" jött, azaz x="b"

```
1   a
2   b
3   c
4   ab
5   by

in: 1,2|4,3,4,8
out: <a b>
```

* Hasonlóan megyünk tovább, most "ab" volt a kódolt egység, és "ab" plusz egy karakter, amit felvettünk. Az is kiderült, hogy az előző körben az "ab" első betűje az y

```
1   a
2   b
3   c
4   ab
5   ba
6   abz

in: 1,2,4|3,4,8
out: <a b ab>
```

* Folytassuk:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cx

in: 1,2,4,3|4,8
out: <a b ab c>
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   ca
8   aby

in: 1,2,4,3,4|8
out: <a b ab c ab>
```

* Most azt látjuk, egy 8-as jön, de a 8-asról még nem tudjuk pontosan mi is
* Segáz, írjuk csak le (s mivel utolsó elem, már nem veszünk fel semmit):

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   ca
8   aby

in: 1,2,4,3,4,8|
out: <a b ab c ab aby>
```

* De ugyanúgy be tudjuk helyettesíteni az y-ba az "a"-t, mint eddig. Tegyünk így és meg is vagyunk:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   ca
8   aba

in: 1,2,4,3,4,8|
out: <a b ab c ab aba>
```

### Dekódolás extrémebb példára

* Most tekintsük az alábbi példát:
* Az ábécé most is az {a,b,c}, a kód pedig ez: **1,2,4,3,5,8,1,10,11,1**
* Dekódoljuk!
* Inicializálás:

```
1   a
2   b
3   c

in: 1,2,4,3,5,8,1,10,11,1
out: <>
```

* Vegyük az első néhány, értelemszerű lépést:

```
1   a
2   b
3   c
4   ax

in: 1|2,4,3,5,8,1,10,11,1
out: <a>
```

```
1   a
2   b
3   c
4   ab
5   by

in: 1,2|4,3,5,8,1,10,11,1
out: <a b>
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abz

in: 1,2,4|3,5,8,1,10,11,1
out: <a b ab>
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cx

in: 1,2,4,3|5,8,1,10,11,1
out: <a b ab c>
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bay

in: 1,2,4,3,5|8,1,10,11,1
out: <a b ab c ba>
```

* Itt vagyunk hasonló helyzetben, mint az előbb, nem ismerjük még teljesen a 8-ast, pedig az jön. Megoldhatjuk úgy, mint legutóbb, írjuk le, abban bízva, hogy utólag tisztul a kép:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bay
9   bayz

in: 1,2,4,3,5,8|1,10,11,1
out: <a b ab c ba bay>
```

* Tisztult, persze. Behelyettesítve y-ba "b"-t:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bab
9   babz

in: 1,2,4,3,5,8|1,10,11,1
out: <a b ab c ba bab>
```

* z-t még mindig nem tudjuk, de nem gond, mehetünk tovább:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bab
9   baba
10  ax

in: 1,2,4,3,5,8,1|10,11,1
out: <a b ab c ba bab a>
```

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bab
9   baba
10  ax
11  axy

in: 1,2,4,3,5,8,1,10|11,1
out: <a b ab c ba bab a ax>
```

* Behelyettesítés:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bab
9   baba
10  aa
11  aay

in: 1,2,4,3,5,8,1,10|11,1
out: <a b ab c ba bab a aa>
```

* Tovább:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bab
9   baba
10  aa
11  aay
12  aayz

in: 1,2,4,3,5,8,1,10,11|1
out: <a b ab c ba bab a aa aay>
```

* Behelyettesítés:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bab
9   baba
10  aa
11  aaa
12  aaaz

in: 1,2,4,3,5,8,1,10,11|1
out: <a b ab c ba bab a aa aaa>
```

* Tovább:

```
1   a
2   b
3   c
4   ab
5   ba
6   abc
7   cb
8   bab
9   baba
10  aa
11  aaa
12  aaaa

in: 1,2,4,3,5,8,1,10,11,1|
out: <a b ab c ba bab a aa aaa a>
```

* És mivel vége, már nem veszünk fel új elemet
