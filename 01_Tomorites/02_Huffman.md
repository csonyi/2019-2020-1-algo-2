# Huffman-kódolás

* Karakterenkénti kód - azaz, minden betűnek bármilyen kontextusban is legyen, mindig ugyanaz a kódja
* Változó kódhossz - azaz, nem feltétlen van minden betű kódja ugyanannyi biten tárolva
    * A gyakoribb karaktereket rövidebben kódoljuk a törmörítés hatékonysága érdekében (akár 1 bit)

## A kódolás menete

* Az input stringen egyszer végigmenve (tehát ennek a műveletigénye lineáris az input hosszának függvényében) egy előfeldolgozással kezdünk, előállítjuk az előzőekben már bemutatott gyakoriságtáblát
* Legyen az előző fejezet példája az itteni példa is:
    * A: 5
    * B: 13
    * C: 17
    * D: 7
    * E: 50
    * F: 8
* Ha a tábla megvan, megkeresem a két legkisebb gyakoriságú elemét, ami itt A és D. Létrehozok két Node-ot (bináris fa csúcsa), megcímkézem a gyakorisággal ill. a betűjelekkel és összehúzom egy szülő csúcsba, amit a két gyakoriság összegével címkézem meg:

```
A5   D7
 \  /
  \/
  12
```

* Most ott tartunk tehát, hogy a két legritkábban előálló karakterből készítettünk egy fát, ami megmondja, mennyi a gyakorisága összesen ennek a két karakternek
* Folytassuk ezt az eljárást! Nézzük végig a táblázat még nem feldolgozott betűit ÉS a készülő gráf fáinak (merthogy nem szükségképpen lesz végig összefüggő a gráf) gyökércsúcsait, válasszuk ki a két legkisebbet, készítsünk hozzájuk Node-okat, húzzuk össze egy szülő Node-ba:
* Ez most az F a 8-as gyakorisággal és a fa 12-es gyakoriságú csúcsa

```
    A5   D7
     \  /
      \/
F8    12
  \  /
   \/
   20
```

* Láthatjuk, teljesen megengedett, hogy a két minimális csúcs a táblázatból jöjjön (lásd első lépés, és lesz még egy ilyen); az is, hogy az egyik a táblázatból, a másik a gráfból (most), és akár az is, hogy mindkettő a gráfból - két részfából -, erre is lesz példa
* Alapvetően, ha megvan a két minimális gyakoriságú karakter / csúcs, mindegy milyen sorrendben írjuk le, a kapott kód más lesz, de a kódolás hatékonysága (azaz a tömörítés mértéke) nem. Én egységesen a táblázatból megyek balról jobbra s utána a fában balról jobbra és így veszem a két minimálist
* Ábrázolás szintjén valid megoldás lehet a gyakoriságlistát prioritásos sorként implementálni, és akár abba eleve egycsúcsú fákként felvenni a betűket, s a kivett-egyesített fákat a megfelelő prioritású helyre visszarakni, addig ismételve, míg csak egy elem nem marad a sorban
* Folytassuk! Most feldolgozatlanul a B-13, C-17, E-50 karakterek, illetve a 20-as Node állnak. Ebből a B és C a minimális, ezért most új részfát szúrúnk be a gráfba, ami a legvégére persze fa lesz:

```
            A5   D7
             \  /
              \/
B13 C17 F8    12
 \  /     \  /
  \/       \/
  30       20
```
* A táblázatban maradt az E-50, nyilván most a két gyökér a minimális:

```
            A5   D7
             \  /
              \/
B13 C17 F8    12
 \  /     \  /
  \/       \/
  30       20
     \   /
      \ /
      50
```
* Végül húzzuk össze a két maradék elemet:
```
              A5   D7
               \  /
                \/
  B13 C17 F8    12
   \  /     \  /
    \/       \/
    30       20
       \   /
        \ /
E50      50
    \   /
     \ /
     100
```
* A végén persze 100-nak kell lennie, hiszen a gyakoriságtábla összes elemét - ami végül is 100 karaktert fed le - beépítettük a fába
* Az így elkészült bináris fát Huffman-kódfának nevezzük, csúcsai az abból elérhető karakterek összgyakoriságával vannak címkézve, a karakterek pedig mindig leveleken vannak
* A fa nem kiegyensúlyozott, minél rövidebb egy ág, annál gyakoribb volt az annak megfelelő karakter
* Ha a fa megvan azt bejárjuk pl. egy veremmel minden lehetséges módon, így minden levélhez (karakterhez) eljutunk
    * Kiírjuk az utakat: úgy vesszük, ha balra megyünk, az él címkéje 0, ha jobbra, 1. Minden érintett él címkéjét kiírjuk a gyökértól a levél felé menve, és az így kapott értelemszerűen egyedi kódot a karakterhez rendeljük:
        * E: 0
        * B: 100
        * C: 101
        * F: 110
        * A: 1110
        * D: 1111
* Maga a kódolás innen egyszerű, ha pl. az "ADE" string része volt az eredeti inputnak (ha csak ennyi lenne az input, más lenne a fa), akkor ennek a substringnek az alábbi a kódja: 111011110

## Hatékonyság

* Alapból a 100 karakteres szöveg 800 biten lenne tárolva
* Itt láthatjuk minden betűhöz, hogy mi a kódja, abból könnyen meg tudjuk állapítani, hogy az hány bites, azt összeszorozva a gyakoriságával pedig megállapíthatjuk annak a betűnek az összes "hosszát" a kódban:
    * E: hossz: 1, db: 50, összesen: 50 b
    * B: hossz: 3, db: 13, összesen: 39 b
    * C: hossz: 3, db: 17, összesen: 51 b
    * F: hossz: 3, db: 8, összesen: 24 b
    * A: hossz: 4, db: 5, összesen: 20 b
    * D: hossz: 4, db: 7, összesen: 28 b
* Összesen tehát 212 bitesre tömörítettük az inputot
* A dekódolhatóság miatt a kódolt változat mellett magát a fát is át kell adni, tehát a kapott teljes átküldendő kód ennél valamivel hosszabb
* Fogadjuk el, hogy rövid szövegre épp ezért ez lehet kevésbé hatékony a naivnál, sőt, akár a tömörítetlen változatnál is

## Dekódolás

* Ha megvan a fa, a következő eljárással dekódolható a kapott bitsorozat:
    * Elindulunk a bitsorozat első karakterétől s a fa gyökerétől, ha az 0, balra, ha 1, jobbra kanyarodunk
    * Ezt folytatjuk, amíg levélhez nem érünk, ekkor kiírjuk a levél címkéjét és visszaugrunk a gyökérhez
* Honnan lehetünk biztosak abban, hogy a dekódolás egyértelmű? Pl. ha a fenti példában egy '0' karaktert látunk, honnan tudjuk, hogy az egy E betű, vagy egy B része?
    * A Huffman-kódolás prefixmentes kódot állít elő, ami azt jelenti, hogy bár az egyik karakter kódja lehet infixe vagy postfixe egy másikénak, de prefixe biztosan nem
    * Azaz, ha egyszer elértünk egy bizonyos karaktert kódoló sorozathoz, megállhatunk, biztosan nem fog ez a kód folytatódni, annak ellenére, hogy elvileg lehet olyan karakter, amit hosszabb kóddal kódolunk, mint a többit
    * Ez a kódolás módszeréből adódik: mindig a levélszintig megyünk, és minden kódot egy egyértelmű út határoz meg. Mivel az út végéig megyünk, nem lehet olyan kód, ami annak a folytatása