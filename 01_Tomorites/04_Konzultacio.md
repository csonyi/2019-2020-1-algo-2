# Algoritmusok és adatszerkezetek 2.

## Első téma - Veszteségmentes adattömörítés - Konzultáció

### Első feladat

* Szemléltessük a Huffman- és az LZW-kódolást a **PERKELEPERKELTPERELT** stringen
* Eredetileg egy byte-os kódolást feltételezve ez a 20 betűs string 20 B helyet foglal
* Mondjuk meg - vezessük le - hány bitre sikerül tömöríteni a Naiv megoldással, illetve a Huffman- és LZW-kódolásokkal

#### Megoldás

##### Naiv (egyszerű) kódolás
* A különböző karakterek: P, E, R, K, L, T; összesen 6 féle
* Minden karakterhez következetesen hozzárendelünk egy egyedi kódot
* 2<sup>x</sup> >= 6, x=3, emiatt az egyedi kódok hossza minden esetben 3
* Pl.: P - 000, E - 001, R - 010, stb.
* Összesen 20 betű van, mindegyike 3 bites, összesen 60 bit (+ az ábécé)

##### Huffman-kódolás
* Készítsük el a gyakoriságtáblát (utolsó oszlop csak ellenőrzés, hogy számoltunk-e minden elemet):
    
```
P E R K L T Össz
3 7 3 2 3 2 20
```

* Vegyük mindig a két legkisebb gyakoriságot, építsünk belőle Node-okat, vonjuk össze, címkézzük meg az új Node-ot a gyakoriságösszeggel
* Amikor már készülnek a fák, a táblázatbeli elemek mellett a fák gyökerei is ugyanúgy "játszanak" a minimumok keresésekor
* Lehet, hogy mindkét minimum a táblázatbó jön, lehet a fából, de lehet vegyesen is

```
K2 T2
 \/
 4
```

```
K2 T2 P3 R3
 \/     \/
 4      6
```

```
  K2 T2 P3 R3
   \/     \/
L3 4      6
 \/
  7
```

* Eddig elég egyértelmű, innen viszont két megoldás is lehet, mindkettőt végigcsinálom:

```
  K2 T2   P3 R3
   \/       \/
L3 4    E7  6
 \/       \/
  7       13
```

```
  K2 T2   P3 R3
   \/       \/
L3 4    E7  6
 \/       \/
  7       13
   \     /
    \   /
     \ /
     20
```

* Innen a gyökértől kezdve az összes ágon a "balra megyek 0, jobbra megyek 1" protokollal bejárva az alábbi kódokat kapom:
    * P - 110
    * E - 10
    * R - 111
    * K - 010
    * L - 00
    * T - 011
* Gyakoriságot szorozva a kódhosszal megkapjuk, hogy az adott karakter összes előfordulása kódolva mennyi helyet foglal:
    * P - 3 * 3 = 9
    * E - 7 * 2 = 14
    * R - 3 * 3 = 9
    * K - 2 * 3 = 6
    * L - 3 * 2 = 6
    * T - 2 * 3 = 6
* Összesen: 50 bit (+ kódfa)
* Ez így teljesen korrekt és jó megoldás. De van még egy, ami nem csak címkézésben, hanem a fa alakjában is más. Mi lesz akkor a kódolási hatékonyság?

```
  K2 T2 P3 R3
   \/     \/
L3 4      6
 \/      /
  7     /
   \   /
    \ /
     13
```

```
  K2 T2 P3 R3
   \/     \/
L3 4      6
 \/      /
  7     /
   \   /
    \ /
E7   13
  \ /
  20
```

* Kódok:
    * P - 110
    * E - 0
    * R - 111
    * K - 1010
    * L - 100
    * T - 1011
* Bitek:
    * P - 3 * 3 = 9
    * E - 7 * 1 = 7
    * R - 3 * 3 = 9
    * K - 2 * 4 = 8
    * L - 3 * 3 = 9
    * T - 2 * 4 = 8
* Összesen: 50 bit (+ kódfa)
* Tehát amíg a szabályokat batartjuk, rugalmas, merre megyünk. Sikerült tömöríteni a naivhoz képest

##### LZW-kódolás

* Az input: PERKELEPERKELTPERELT
* A kezdeti szótár:

```
1 P
2 E
3 R
4 K
5 L
6 T
```

* Lépésről lépésre:

```
in = P|ERKELEPERKELTPERELT
1 P
2 E
3 R
4 K
5 L
6 T
7 PE
out = <1>
```

```
in = PE|RKELEPERKELTPERELT
1 P
2 E
3 R
4 K
5 L
6 T
7 PE
8 ER
out = <1,2>
```

```
in = PER|KELEPERKELTPERELT
1 P
2 E
3 R
4 K
5 L
6 T
7 PE
8 ER
9 RK
out = <1,2,3>
```

```
in = PERK|ELEPERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
out = <1,2,3,4>
```

```
in = PERKE|LEPERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
out = <1,2,3,4,2>
```

```
in = PERKEL|EPERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
out = <1,2,3,4,2,5>
```

```
in = PERKELE|PERKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
out = <1,2,3,4,2,5,2>
```

```
in = PERKELEPE|RKELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
out = <1,2,3,4,2,5,2,7>
```

```
in = PERKELEPERK|ELTPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
out = <1,2,3,4,2,5,2,7,9>
```

```
in = PERKELEPERKEL|TPERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
out = <1,2,3,4,2,5,2,7,9,11>
```

```
in = PERKELEPERKELT|PERELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
17 TP
out = <1,2,3,4,2,5,2,7,9,11,6>
```


```
in = PERKELEPERKELTPER|ELT
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
17 TP
18 PERE
out = <1,2,3,4,2,5,2,7,9,11,6,14>
```

```
in = PERKELEPERKELTPERELT|
1  P
2  E
3  R
4  K
5  L
6  T
7  PE
8  ER
9  RK
10 KE
11 EL
12 LE
13 EP
14 PER
15 RKE
16 ELT
17 TP
18 PERE
out = <1,2,3,4,2,5,2,7,9,11,6,14,16>
```

* Az output 13 karakteres, egy 18 elemű ábécén, azaz minden egyes számot x biten kódolunk, ahol 2<sup>x</sup> >= 18
* x = 5, tehát 13 * 5 = 65 bit a kód hossza, plusz az ábécé
* Ez nem egy jó tömörítés erre a példára

### Második feladat

* Írjuk meg az LZW dekódolás algoritmusát

#### Megoldás

```
lzwDecode(in : InStream) : OutStream
    out := <>
    stringTable := initFromAlphabet()
    c := in.read()
    current := stringTable.get(c)
    out.print(current)
    c := in.read()
    AMÍG c != EOF
        HA c eleme stringTable
            next := stringTable.get(c)
        KÜLÖNBEN
            next := current + current₁
        out.print(next)
        stringTable.add(current + next₁)
        current := next
        c := in.read()
    return out
```

* Az inputunk most a kód, a konzolra pedig az eredeti stringet szeretnénk kiírni
* az initFromAlphabet()-et már ismerjük, úgy általában a kódolást akarjuk "visszafelé" megvalósítani
* a stringTable.get() itt most kódhoz rendel stringet, nem stringhez kódot, az add() ugyanazt csinálja, mint a kódolós algoritmusnál
* current₁ jelentse a current szó első karakterét
* Feltételezzük, hogy a kód helyes és nem üres
* Az elején kiolvassuk az első kódot. Ez biztosan az ábécé egyik eleme, hiszen kezdetben ezek voltak csak a szótárban, ezeket tudtuk kódolni
* Amikor elkezdődik a ciklus, az alábbiakat tudjuk:
    * Ki van írva az első betű
    * c-ben a második betű (vagy itt már lehet akár részstring is) kódja
    * currentben az első betű
* Általában, c-ben az utolsó feldolgozott kód utáni kód van, azaz, ha ez EOF, kiléphetünk
* Ha nem EOF, és benn van a szótárban, akkor kérjük le, hogy minek a kódja ő, ezt írjuk is ki (hiszen ebből az inputból lett ez a kód) - ez a next változó, majd gondoljuk el azt, hogy amikor még az előző kódot írtuk ki (ne feledük, a ciklus előtt azt már dekódoltuk!), akkor még adósak vagyunk azzal, hogy az annál eggyel hosszabb stringet elkódoljuk. Ezt megtesszük most, a currentben volt az előző kódolandó string, a nextben a mostani, az előzőt felvesszük a next első karakterével megtoldva, majd mivel mindent megtettünk, tovább is léphetünk. Ha épp az utolsó karakternél vagyunk, azt kiírjuk, majd a továbblépésnél kilépünk a ciklusból, azaz az utolsó karakter által kódolt szó + a következő által kódolt szó első karakterét nem fogjuk már kiírni a szótárba, hiszen ilyen nincs is
* Érdekesebb eset a másik ág. Feltételezzük, hogy az input (a teljes kód) helyes. Tehát ha az a kódindex, ami most még nincs a string táblában egy helyes kódolt részstring volt mégiscsak, akkor feltételezhetjük, hogy amikor a kódolást csináltuk, már ismert volt a string táblában ez a kód. Hogy lehet ez? A kódolás minden lépése után felveszünk egy elemet. Az első lépés után tehát |ábécé|+1, a k. lépés után |ábécé|+k a szótár elemszáma. Megjegyzem, az utolsó után nem veszünk fel, tehát végül |ábécé|+|lépések|-1 lesz a méret
    * De ezt a kódolásnál látott működést hogy reprodukáljuk a dekódolásnál? Láthattuk az előző ágon: amikor kiírjuk a k. elem kódját, akkor (mivel csak mostanra tudtuk meg a teljes értékét) vesszük fel az k-1. elemnél eggyel hosszabb stringhez tartozó kódot, azaz az |ábécé|+k-1-edik kódot. Azaz amikor elágazásunk ehhez a sorhoz ér a k. karakternél, a string tábla mérete még csak |ábécé|+k-2, hiszen a stringTable.add() még csak később jön
    * Kódoláskor a k. lépésnél keletkezik az |ábécé|+k. indexű bejegyzés, de kiírni maximum csak az |ábécé|+k-1-es indexű kódot fogjuk (hiszen előbb volt a kiírás és utána a felvétel)
    * Dekódoláskor mivel eggyel el vagyunk csúszva - helyes inputot feltételezve -, ez az egyetlen olyan kódindex, amivel úgy találkozhatunk, hogy még nem ismerjük (pedig kódoláskor ismertük). Nagyobb egyszerűen nem lehet, a kisebbeknél pedig a másik ágra futunk
    * Viszont ha ez az |ábécé|+k-1. kód, akkor a k-1. lépésben született. Ebben a lépésben a k-1. substringet írtuk ki kódolva és a k. elem első betűjét konkatenálva a végére kódoltuk el és vettük fel, mint |ábécé|+k-1 kódú elem
    * Tehát tudjuk azt, hogy dekódoláskor a k. lépésben velünk inputként szembejövő |ábécé|+k-1. kódindex a k-1. lépés dekódolt stringjénél "eggyel nagyobb". A k-1. lépés dekódolt stringje pedig a current változó értéke
    * Ehhez a current változóhoz csapjuk most hozzá ezt a stringet..., amiről tudjuk, hogy a current változóval kezdődik. Egész pontosan nem más, mint a current változó, plusz a következő string első betűje, na de a következő string első betűje önmaga, ha nem így lenne, akkor a másik ágon lennénk. Emiatt mondható, hogy a next nem lesz más, mint a current + a current első betűje
* Az órai példában ilyenkor volt az, hogy leírtam a megfelelő betűk helyére "ismeretleneket" (x,y,z), amiket később visszahelyettesítettem. Volt olyan eset, amikor az utolsó két karakter is ismeretlen volt. Ebből az utolsó igazából csak a pufferelés miatti még nem beolvasott nextbeli első karakter, amit ennek az algoritmusnak az előreolvasó mivolta meg is old, a másik ilyen ismerelten karakter pedig mindig azon esetben jön elő, amikor a dekódolandó kód épphogy az előbbi lépésben lett felvezetve a táblába. Ezért ilyenkor nem is kell az ismeretlenekkel számolgatni, hanem az algoritmus alapján kimondható, hogy az új kód utolsó karaktere egyben az előző kód első karaktere. Ezt az igazságot akár a lejátszásnál is alkalmazhatjuk mostantól
