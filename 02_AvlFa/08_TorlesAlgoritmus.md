# AVL-fa - Törlés

* Az alábbiakban - az előadásanyag nyomán - lássunk egy igen összetett algoritmust amely a paraméterként megadott AVL-fából előbb megkeresi, majd törli a szintén paraméterként megadott kulcsú csúcsot, majd ezek után helyreállítja az egyensúlyt
* A törlést úgy végezzük el, hogy kétgyerekes részfa esetén a törlendő elem helyére a bal részfa legnagyobb eleme kerül - ebben, és ennek néhány következményében el fogunk térni az előadásanyagban található verziótól

```
delete(&t : Node*, k : T, &d : L)
    HA t = NULL
        d := false
    KÜLÖNBEN
        HA k < t->key
            delete(t->left, k, d)
            HA d
                leftSubtreeShrunk(t, d)
        HA k > t->key
            delete(t->right, k, d)
            HA d
                rightSubtreeShrunk(t, d)
        HA k = t->key
            deleteRoot(t, d)
```

* Az algoritmus rekurzív
    * Ha a keresett k kulcsnál a kiinduló (vagy a rekurzió során elért) fa gyökere nagyobb, a fa bal részfája, ha kisebb, a jobb részfája felé megyünk tovább
    * Ha elértük a gyökeret, az általa definiált fa csúcsát kell törölnünk
    * Ha a fa üres volt, vagy elértük az üreset (azaz nem volt benne a k kulcs), akkor kilépünk
* Egy d nevű változót hordozunk végig az algoritmuson
    * Ez azt jelenti, hogy az adott rekurziós szinten a t-vel jelölt fa magassága csökkent-e. Ha igen, akkor lehet, hogy a következő rekurziós szinten is kell valamit kezdenünk, ha nem, nem
    * Azt biztosan tudjuk, ha a keresett elem nincs a fában, a magasság se csökkent. Ezt jelzi d hamisra állítása
    * Ha d igaz, majd akkor kell forgatásokat végeznünk
* t-t referencia szerint adjuk át, hiszen potenciálisan töröljük, azaz átmutattatjuk máshova az őt jelző pointert; d-t is így adjuk át, hiszen értéke változhat, aminek ki kell hatnia a magasabb rekurziós szintekre
* Java nyelven legalábbis erre az algoritmusra egy osztályt írnék, ahol a t és a d is adattag lenne, így nem kéne minden algoritmusba átadogatni

```
deleteRoot(&t : Node*, &d : L)
    HA t->left = NULL
        p := t
        t := p->right
        delete p
        d := true
    HA t->right = NULL
        p := t
        t := p->left
        delete p
        d := true
    HA t->left != NULL és t->right != NULL
        leftSubtreeMaxToRoot(t, d)
        HA d
            leftSubtreeShrunk(t, d)
```

* Itt nem ellenőrzünk NULL-t és nem adtuk át k-t sem, mert a fő algoritmusból tudjuk, hogy t nem NULL és a kulcsa k
* Ha nincs két gyerek, csak "felhúzzuk" az egyik gyereket a gyökér helyére, majd az eddigi gyökeret töröljük. Ha annak a gyereknek volt bármilyen gyereke, az "öröklődik" az által, hogy nem rontjuk el annak pointereit - ezért szerencsés itt a láncolt ábrázolás
    * Ha egy gyerek sincs, random az első vagy a második ág fut le (a struktogram-nyelvben a többágú elágazás nemdeterminisztikus), ami igazából kicsit úgymond felesleges lépésekkel, de szabályos eredményre vezet
    * Ekkor d értéke biztos igaz lesz: ha nem volt se bal- se jobb gyerek, akkor konkrétan egy csúcs volt, amit töröltünk -> csökkent a méret. Ha volt valamelyik gyerek, de csak az egyik, akkor azt felhúztuk, minden feljebb került -> megint csak csökkent a méret    
    * Ha d értéke igaz, az azt jelenti, hogy fentebb lehet, hogy lesz forgatás, de az is biztos, hogy ezen a szinten nem lesz. Ha nem volt egy gyerek se, egy levélcsúcsot töröltünk, ami miatt nincs minek forognia; ha pedig volt gyerek, de csak egy, akkor annak már nem lehetett gyereke, különben t "--"-os vagy "++"-os lett volna eddig is
* A harmadik ág a legérdekesebb, ha volt mindkét gyerek. Ekkor persze terebélyes részfák is lehetnek. A törlés után a törölt csúcsot a keresőfa-tulajdonság miatt az őt inorder bejárás szerint megelőző (bal részfa maximuma), vagy rákövetkező (jobb részfa minimuma) csúccsal cseréljük és értelemszerűen láncoljuk át a többi csúcsot (ez lesz a következő algoritmus). Utána helyreállítjuk az egyensúlyokat. Az itteni példában a bal részfa maximumát használjuk, az előadáson a jobb részfa minimumáról volt szó
    * Ezen az ágon nem feltétlen csökken a fa magasság - akkor van ez így, ha a törölt csúcs szülőjének balance-a 0 volt. Ekkor volt testvére a törölt csúcsnak, ezért a magasság végül nem csökken

```
leftSubtreeMaxToRoot(&t : Node*, &d : L)
    p := NULL
    removeMax(t->left, p, d)
    p->left := t->left
    p->right := t->right
    p->b := t->b
    delete t
    t := p
```

* Bevezetünk egy p-t, amibe a második sor segédfüggvénye majd belerakja a t balrészfájából kiinduló részfa maximumát
* Ez az algoritmus majd le is választja p-t annak szülőjéről, és annak helyére p bal részfáját teszi - amit így p-ről választ le
* Azaz t->left egy szabályos keresőfa marad a maximuma nélkül, p-ben pedig csak ez az egy csúcs van
    * A rightja is biztosan NULL, mert különben ha nem úgy lenne, nem p lett volna a maximum
* Ekkor most t minden attribútumát p-re ruházzuk, a végén még a mutatót is, és töröljük a régi gyökeret (elvégre eredetileg ezt akartuk törölni, nem p-t, azt csak "kiemeltük" - innen a különbség a függvények neveiben: delete vs. remove)
* Fontos, hogy adattagonként másoljuk át p-be t dolgait, mert nem tudhatjuk, hogy a kulcs és főleg a többi járulékos adattag másolása mivel járna

```
rightSubtreeShrunk(&t : Node*, &d : L)
    HA t->b = -1
        balanceMM(t, d)
    KÜLÖNBEN
        t->b := t->b - 1
        d := t->b = 0
```

* Ezt a függvényt - mint láthattuk - akkor hívjuk meg, ha valamelyik rekurzív hívásnál a jobb részfa csökkent
    * Ekkor megnézzük, a szülő csúcs már eddig is "-"-os volt-e. Ha igen, akkor nem is állítjuk "--"-ra (így spórolunk, effektíve csak a "0", "-", "+" értékek jelennek meg b-ben), hanem egy "MM", azaz "--" forgatást végzünk (hogy melyik forgatást pontosan, az majd a segédfüggvényben kiderül)
    * Ha pedig nem volt "-"-os, akkor csökkentjük. Most két dolog lehet
        * Vagy most lett "-"-os (azaz -1-es). Ekkor eddig 0-s volt: volt két egyforma gyereke, az egyik kisebb lett, de a t magassága nem csökkent így, tehát d hamis lesz
        * Vagy most lett "="-s (azaz 0-s). Ekkor eddig 1-es volt, tehát a jobb részfa utolsó elemét töröltük ki: ezzel a jobb részfa csökkent, de a bal eleve eggyel kisebb volt, tehát az egész építmény mérete csökkent. Ugyan itt "--" nincs (az az első ág volt), de a szülőnél akár "+", akár "-" ha volt, abból most kétszeres kiegyensúlyozatlanság lehet, hiszen az egyik gyereke kisebb lett. Ezért ilyenkor d igaz lesz, és visszatérünk a szülőhöz a rekurzióval

```
lefttSubtreeShrunk(&t : Node*, &d : L)
    HA t->b = 1
        balancePP(t, d)
    KÜLÖNBEN
        t->b := t->b + 1
        d := t->b = 0
```

* A teljesség igénye miatt megadtam ezt is, láthatók a kis különbségek
* Most nézzük a maximumot kivevő algoritmust:

```
removeMax(&t : Node*, &maxp : Node*, &d: L)
    HA t->right = NULL
        maxp := t
        t := maxp->left
        maxp->left := NULL
        d := true
    KÜLÖNBEN
        removeMax(t->right, maxp, d)
        HA d
            rightSubtreeShrunk(t, d)
```

* Itt ha már nincs jobb gyerek (akinek a kulcsa nagyobb, mint t kulcsa), akkor t a maximum
    * t-t kimentjük maxp-be, de t-be (ami a szülő left vagy right pointere volt) kimentjük maxp bal gyerekét (jobb nincs neki)
    * Ekkor biztosan csökkent a t magassága, mivel egy üres jobb részfa helyére húztunk fel egy részfát
* A másik ágon megy a rekurzió, addig megyünk jobbra, amíg csak tehetjük
* A végén csak akkor kellhet forgatni, ha d igaz..., de d-t igazra állítjuk, akkor miért nézzük ezt meg? Azért mert d-t egyszer állítjuk igazra a bal ágon, utána már a rekurzió visazfejétsekor épp a rightSubtreeShrunk állíthatja hamisra
* A forgatások maradtak már csak

```
balanceMM(&t : Node*, &d : L)
    l := t->left
    HA l->b = -1
        balanceMMm(t,l)
    HA l->b = 0
        balanceMM0(t,l)
        d := false
    HA l->b = 1
        balanceMMp(t,l)
```

* "--"-os forgatásokkor a bal gyerek egensúlya alapján dől el, hogy (--,-) (első ág), (--,=) (második ág), vagy (--,+) lesz-e (harmadik ág)
* Ide már úgy érkezünk, hogy d igaz, a (--,=) ezen változtat: hiszen ott a részfák mérete nem csökken, a másik kettőnél igen (és potenciálisa tovább is kell folytatni a helyreállításokat)
* Azt is vegyük észre, hogy eddigre az l balance-a már frissítve lett

```
balanceMMm(&t : Node*, l : Node*)
    t->left := l->right
    l->right := t
    l->b := 0
    t->b := 0
    t := l
```

* Itt láthatjuk, hogy a t és l kvázi helyet cserél, csak persze a rendezettség miatt az irány változik
* A balance-okat a forgatás definíciójából tudjuk
* A részfák persze jönnek a cserélt Node-okkal

```
balanceMM0(&t : Node*, l : Node*)
    t->left := l->right
    l->right := t
    l->b := 1
    t->b := -1
    t := l
```

* Hasonló
* És végül:

```
balanceMMp(&t : Node*, l : Node*)
    r := l->right
    l->right := r->left
    t->left := r->right
    r->left := l
    r->right := t
    l->b := alsóegészrész((r->b+1)/2)
    t->b := alsóegészrész((1-r->b)/2)
    r->b := 0
    t := r
```

* Le kell játszani, és akkor világos lesz mi miért. Gyakorlatilag itt 3 csere van, mint mindig, itt is a sorrend ami nagyon fontos, ne legyen memóriaszivárgás