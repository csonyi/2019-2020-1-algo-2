# Algoritmusok és adatszerkezetek 2.

## Második téma - AVL-fák - konzultáció

### Első feladat

* Készítsünk AVL-fát az alábbi lépések egymás utáni végrehajtásával (i – insert, d – delete). Minden lépést, ha kell, forgatásokat jelöljünk.
    * i - 10, 6, 13, 8, 15, 7, 20, 4, 21, 14, 12, 11
    * d - 7
    * i - 9
    * d - 14

#### Megoldás

* i - 10

```
10
```

* i - 6

```
 10-
 /
6
```

* i - 13

```
 10=
 /\
6 13
```

* i - 8

```
 10-
 / \
6+ 13
 \
  8
```

* i - 15

```
 10=
 / \
6+ 13+
 \  \
  8  15
```

* i - 7

```
 10=
 / \
6++ 13+
 \   \
  8-  15
  /
  7
```

* (++,-) forgatás

```
  10=
  / \
 7=  13+
/ \   \
6  8   15
```

* i - 20

```
  10=
  / \
 7=  13++
/ \   \
6  8   15+
        \
         20
```

* (++,+) forgatás

```
   10=
  /   \
 7=    15=
/ \    / \
6  8  13 20
```

* i - 4

```
    10-
   /   \
  7-    15=
 / \    / \
 6- 8  13 20
/
4
```

* i - 21

```
    10=
   /   \
  7-    15+
 / \    / \
 6- 8  13 20+
/           \
4           21
```

* i - 14

```
    10=
   /   \
  7-    15=
 / \    / \
 6- 8  13+ 20+
/       \   \
4       14   21
```

* i - 12

```
    10=
   /   \
  7-    15=
 / \    / \
 6- 8  13= 20+
/      / \    \
4     12  14  21
```

* i - 11

```
    10+
   /   \
  7-    15-
 / \    / \
 6- 8  13- 20+
/      / \    \
4     12- 14  21
      /
     11
```

* d - 7

```
    10+
   /   \
  8--   15-
 /      / \
 6-    13- 20+
/      / \    \
4     12- 14  21
      /
     11
```

* (--,-) forgatás

```
    10++
   /   \
  6=    15-
 / \    / \
4   8  13- 20+
       / \    \
      12- 14  21
      /
     11
```

* (++,-) forgatás

```
      13=
    /     \
   10=     15+
  /  \     / \
 6=   12- 14  20+
/ \   /       \
4  8 11        21
```

* i - 9

```
      13-
    /     \
   10-     15+
  /   \     / \
 6+    12- 14  20+
/ \    /         \
4  8+  11         21
    \
     9
```

* d - 14

```
      13-
    /     \
   10-     15++
  /   \       \
 6+    12-     20+
/ \    /         \
4  8+  11         21
    \
     9
```

* (++,+) forgatás

```
      13--
    /     \
   10-     20=
  /   \     / \
 6+    12- 15  21
/ \    /
4  8+  11
    \
     9
```

* (--,-) forgatás


```
        10=
      /    \
     /      \
    6+       13=
   /  \      / \
  4    8+   12- 20=
        \   /  / \
         9 11 15 21
```

### Második feladat

* a) Egy általad választott n természetes számhoz megadható-e úgy egy n méretű AVL-fa és egy felső korlát egy kulcssorozat hosszára, hogy ezen sorozat minden elemét rendre a fába szúrva, annak magassága ne változzon? Miért?
* b) Ugyanez, ha n>0

#### Megoldás

* a)
    * A fa "mérete" az elemszámát jelenti, a magasság pedig pongyolán a leghosszabb út hossza -1 (azaz üres fánál -1, egy csúcsú fánál 0, stb.)
    * Innen ha n=0, annak -1 a magassága, és bármit is szúrúnk be egy ilyen fába, annak 0-ra nő a magassága (és 1-re a mérete)
    * Azaz n=0 esetben a sorozat legfeljebb 0 hosszú lehet
* b)
    * Bármi is az n, ha n>0, akkor legalább egy csúcsa van a fának. Ha most ezt a csúcsot akarjuk újra és újra beszúrni, az algoritmus nem fog csinálni semmit
    * Ebből következően tetszőleges x gyökerű nemüres fára az <x, x, x, ...> végtelen hosszú sorozat olyan, hogy minden elemét sorban beszúrva a fába, nem változik annak magassága. Azaz ez esetben nincs felső korlát

### Harmadik feladat

* Írjunk algoritmust, ami a paraméterként átadott láncolt ábrázolású AVL-fáról eldönti, hogy a szintén paraméterként átadott kulcshoz tartozó csúcsról tárolt egyensúly érték helyes-e
* Ha a kulcs nem szerepel a fában, térjünk vissza igazzal

#### Megoldás

* Itt azt kell tudni, hogy az AVL-fáknál a "b" adattagban tároljuk az egyensúlyt, aminek helyes fa esetén 3 értéke lehet: -1 (ha a bal részfa eggyel magasabb), 0 (ha egyenlők - de persze a részfák már lehetnek kiegyensúlyozatlanok), 1 (ha a jobb magasabb)
* Megkeressük előbb az érintett csúcsot, utána megállapítjuk a gyerekei magasságát:

```
checkBalance(t : Node*, k : T) : L
	t := find(t, k)
	HA t = NULL
		return true
	KÜLÖNBEN
		return t->b = h(t->right) - h(t->left)
```

```
find(t : Node*, k : T) : Node*
	HA t = NULL
		return NULL
	KÜLÖNBEN
	    HA t->key > k
		    return find(t->left, k)
	    HA t->key < k
		    return find(t->right, k)
	    HA t->key = k
		    return t
```

```
h(Node* t) : Z
	HA t = NULL
		return -1
	KÜLÖNBEN
		return max(h(t->left), h(t->right)) + 1
```

```
max(a : Z, b : Z) : Z
	HA a > b
		return a
	KÜLÖNBEN
		return b
```
