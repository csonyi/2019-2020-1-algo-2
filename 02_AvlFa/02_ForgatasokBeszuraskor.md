# AVL-fa - Beszúrás

* Az alábbiakban példát láthatunk arra, hogy építhetünk fel kulcsokból egy AVL-fát
* A kiegyensúlyozottság elrontása esetén 4 lehetséges forgatási sémát alkalmazhatunk
* Az alábbi ábrákon a latin betűk mindig egy csúcsot, a görög betűk mindig egy (akár üres) részfát ábrázolnak a megadott magasságokkal

## A (++,+) forgatás

* Előfordulhat egy beszúrás után, hogy a beszúrt csúcsból a gyökér felé indulva vezető úton egy ilyen részfát látunk:

```
    x
   / \
  α   y
     /\
    β  γ
```

* Ahol az α és a β magassága h, a γ-é pedig h+1, következésképpen az x bal részfájáé h, a jobbé h+2 (hiszen ott van y + annak a nagyobb részfája, aminek h+1 a magassága)
* Vezessünk be egy "balance" adattagot a csúcsokhoz, amit az alábbi módon definiálunk:
    * "++" - ha a jobb részfa magassága kettővel nagyobb, mint a balé
    * "+" - ha a jobb részfa magassága eggyel nagyobb, mint a balé
    * "=" - ha a két részfa magassága egyenlő (ami nem jelenti azt, hogy rekurzívan is mindegyik egyenlő)
    * "-" - ha a bal részfa magassága eggyel nagyobb, mint a jobbé
    * "--" - ha a bal részfa magassága kettővel nagyobb, mint a jobbé
        * Az nyilvánvaló, hogy más eset nem lehetséges: a beszúrás előtt csak 1 lehet a különbség, azaz a +, =, - címkék fordulhatnak elő, s ezt a beszúrás elronthatja "++"-ra vagy "--"-ra
        * Az nem feltétlen igaz, hogy rögtön a beszúrt csúcs szülőjének a kiegyensúlyozottsága romlik el, de az egész biztos, hogy a beszúrt csúcsból a gyökér felé vezető úton romlik el valahol, már ha egyáltalán
        * Ennek megállapítása maximum logaritmikus, a helyrehozás pedig konstans műveletigényű
        * Azt majd látni fogjuk, hogy az összes most bevezetett forgatás effektíve csökkenti a forgatásban résztvevő részfa magasságát. Mivel a magasságkülönbség épp a beszúrás nyomán jött létre, egy ilyen forgatás ezt a különbséget egyenlíti ki, ezért ha kell is helyreállítási műveletet végezni, biztosak lehetünk benne, hogy a beszúrás nyomvonalán visszafelé haladva legfeljebb egy ilyen műveletre lesz szükség
* A fentiek alapján y balance-a "+", x balance-a "++"
* Ezért hívjuk ezt (++,+) forgatásnak
* A forgatás után az alábbi állapot jön létre:

```
    y
   / \
  x   γ
 /\
α  β
```

* Most x egyensúlya a két h magas részfa miatt "=", y-é pedig a h+1 magas jobb részfa és az "x + h magas" bal részfa miatt szintén "="
* Persze a görög betűkkel jelölt részfákban az esetleges nemegyenlő egyensúlyok megmaradhattak, de a lényeg, sehol sem lesz dupla plusz/mínusz
* Mit is csináltunk?
    * y bal részfáját x jobb részfájává tettük (megtehettük, mert a keresőfa-tulajdonság miatt ez y-nál már csupa kisebb, de x-nél (hiszen neki y a jobb gyereke volt) még csupa nagyobb elemet tartalmaz)
    * x jobb részfáját (azaz y-t) megtettük x szülőjének, vagy másképpen mondva y bal gyerekévé tettük x-et (kulturális referencia: https://www.youtube.com/watch?v=AXUADjjuPU8) (ezt is megtehettük, mert ha y nagyobb, mint x, akkor x, kisebb, mint y)
* Láthatjuk tehát, két átmutattatást végeztünk abban az irányban, amerre a kiegyensúlyozatlanság létrejött
* A többi részfa megmaradt balról jobbra ugyanabban a sorrendben, csak értelemszerűen a két szülő kivételével a másik kettő szülőt cserélt
* Azt se feledjük, hogy ha ezt a fát a t pointer fogta, ez eredetileg x-re, a végén y-ra mutat (ígyszólván 3 átmutattatás van)
* A lényeg, a keresőfa-tulajdonság megmaradt, másképpen fogalmazva az inorder bejárás eredménye ugyanúgy növekvő sorrendben sorolja fel a csúcsok címkéit
* A leírtakat érdemes néhányszor végiggondolni, s így a séma könnyen memorizálható

## A (--,-) forgatás

* A (++,+) tükörországbeli testvére, így kommentár nélkül a lényeget írom csak le:

```
    x
   / \
  y   γ
 /\
α  β
```

* α magassága h + 1, β magassága h, ezért y x bal részfájának magassága h+2
* γ magassága h, ezért x jobb részfájának magassága h
* x ezért "--"-os, y pedig "-"-os
* A forgatás során megint y és x cserél helyet, e kettő csúcs egyensúlya "="-vé válik:

```
  y
 / \
α   x
    /\
   β  γ
```

## A (++,-) forgatás

* Alaphelyzet:

```
  x
 / \
α   y
    /\
   z  δ
   /\
  β  γ
```

* α és δ magassága h
* β és γ magassága pedig rendre h és h-1; vagy h-1 és h; vagy h és h
    * h-1 és h-1 nem lehet, mert akkor y "="-s lenne, és az már más eset
* z kiegyensúlyozottsága bármi lehet a megengedett 3-ból
* y bal részfája viszont mindenképp h+1 magas, a jobb pedig h, ezért y "-"-os
* x bal részfája h magas, jobb részfája pedig y magasabbik részfája + 1, azaz h+2 magas, azaz x "++"-os
* Innen a forgatás neve

```
     z
   /   \
  x     y
 / \   / \
α   β γ   δ
```

* Itt z mindkét gyerekét cseréljük; x bal gyereke és y jobb gyereke marad; z gyerekei a keresőfa-tulajdonság alapján besorolnak x és y alá
* Illetve a fa gyökere már nem x, hanem z, tehát összesen 4 (2 csere) + 1 átpointerezés van
* A balance-ok is frissültek: x-é "-" vagy "="; y-é "+" vagy "="; z-é biztosan "=" lesz
* Ez is csökkenti a részfa magasságát akár a (++,+)/(--,-) páros

## A (--,+) forgatás

* Just for the record:

```
    x
   / \
  y   δ
 /\
α  z
   /\
  β  γ
```

* α és δ magassága h
* β és γ magassága h és h-1; vagy h-1 és h; vagy h és h
* Azaz x balance-a "--"
* y balance-a "+"
* z balance-a irreleváns

```
     z
   /   \
  y     x
 / \   / \
α   β γ   δ
```

* z balance-a "="
* x balance-a "+" vagy "="
* y balance-a hasonlóan "-" vagy "=". A "+" féknyúz
