# AVL-fa - Törlés

## Konkrét példa

* Induljunk ki abból a fából, amit az előző, beszúrásos példa során építettünk:

```
       73
     /    \
   67      77
  /   \     /\
 15    70  75 80
/ \    / \      \
10 33  69 71     82
  /        \
 20        72
```

* Töröljük a 75-ös csúcsot!

```
       73
     /    \
   67      77++
  /   \      \
 15    70     80+
/ \    / \      \
10 33  69 71     82
  /        \
 20        72
```

* Na, ezzel jól meg is csináltuk a bajt, a szülője "++"-os lett. Mivel a másik gyereke "+"-os, ezért a (++,+) forgatással kell javítanunk a tárgyalási pozíciónkon
* Utána meg kell néznünk, a szülő, elromlott-e. Igen:

```
       73--
     /    \
   67=     80
  /   \    / \
 15    70 77  82
/ \    / \
10 33  69 71
  /        \
 20        72
```

* Itt nem az út mentén kell nézni a forgatás "szekunder" csúcsát, ami a szintén "="-s 80-as lehetne, hanem illesszük rá a szituációra a sémát: a (--,=) séma y csúcsa x-től balra van, ezért itt a 67-es úton indulunk el... (ez életszerű is, hiszen arra nehezebb a fa, ott kell egyensúlyozni)

```
       67
     /    \
   15      73
  /  \    /  \
 10  33  70   80
    /   / \   / \
  20   69 71 77 82
           \
            72
```

* Följebb már nincs mit csekkolni, mert elfogyott a fa, de amúgy se kéne, mert nem csökkent a magasság