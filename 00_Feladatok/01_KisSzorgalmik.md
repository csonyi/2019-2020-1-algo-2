# Info

* Minden "határidő"-t értsünk úgy, hogy a megadott nap 23:59 időpontjáig adható le a kész munka
* A megoldást a canvas felületen adjátok le (méltányolható okokból lehet kivétel, ekkor emailben kérem)
* A beadás formája:
    * Algoritmusírás esetén pszeudokód vagy struktogram
    * Osztályok esetén osztálydiagram + algoritmusok (lásd fent)
    * Lejátszós feladat esetén az órai stílushoz hasonló lépésről lépésre illusztrált lejátszás
    * Valamint az összesre vonatkozóan: írhatok hozzá mindenféle szöveges magyarázatot, ha a feladat ezt kéri, vagy Ti szükségesnek érzitek
    * Programnyelvi kódot itt nem tudok elfogadni - de fogok kiírni célzottan kódolós feladatokat is
* A határidőn túl beadott megoldásokat is átnézem, canvasban leírom, milyen lett - tanulni lehet belőle -, de arra pontot már nem adok
* Amennyiben megcsúszok a félév végén, a globális határidő mindenre: 2020. 01. 31., 16:00
* Hacsak más nincs mondva, minden feladat 1 pontot ér

# Veszteségmentes adattömörítés

## 01_HuffmanFreqTable

* Készítsd el a Huffman-kódolás gyakoriságtáblájának megfelelő FreqTable típust. Adj reprezentációt, valamint implementációt a következő műveleteknek:
	* addFreq(Char) - A paraméterként megadott karakter számosságát növeli a táblában. Ha még nem volt ilyen karakter, 1-re inicializálja a számosságot
	* getFreq(Char) - visszaadja a megadott karakter gyakoriságát (0 is lehet)
	* decFreq(Char) - Csökkenti eggyel a megadott karakter gyakoriságát (0 esetében marad 0)
	* remFreq(Char) - Nullázza a megadott karakter gyakoriságát
	* remMinFreq() : (Char, N) - Kiveszi a legkisebb gyakoriságú elemet a táblából, visszatér annak nevével és számosságával egy pár formájában. Előfeltétel: ne legyen üres a tábla
	* remMaxFreq() : (Char, N) - Kiveszi a legnagyobb gyakoriságú elemet a táblából, visszatér annak nevével és számosságával egy pár formájában. Előfeltétel: ne legyen üres a tábla
	* size() : N - Megadja, hány féle karaktert tárolunk a táblában
	* totalFreqs() : N - Megadja a reprezentált string hosszát
* Legyen adott egy MAX_SIZE : N konstans. Feltehetjük, hogy nem lesz ennél több elemű ábécével dolgunk
* Bármilyen előző félévben tanult reprezentációt használhatsz,  műveleteket egészen elemi szintig fejtsd ki
* Törekedj a műveletigények minimalizására rendeltetésszerű (Huffman-kódoláshoz való) használat esetén
* Röviden indokold meg, miért ezt a reprezentációt választottad, hasonlítsd össze, más szóba jöhető reprezentációkkal
* Érték: 2p
* Határidő: 2019. 10. 14.

## 02_HuffmanTree

* Írj algoritmust, ami egy (akár üres) InStream formában megadott kódolandó string paraméterből előállít egy annak megfelelő Huffman-kódfát
* A fa legyen az előző félévben tanult láncolt reprezentációjú bináris fa - azaz a Node típust használjuk
* A csúcsok címkéje (kulcsa) most legyen egy pár: a betű, amit kódol (nem-levél csúcsok esetén üres karakter), és az adott csúcs által lefedett gyakoriság
* Élcímkék ne legyenek
* Tegyük fel, hogy adott a FreqTable típus az előző feladatban definiált műveletekkel (ezeket megvalósítani tehát nem kell, de használhatók)
* Törekedjünk a minimális műveletigényre, feltéve, hogy a gyakoriságtábla műveletei is optimálisan vannak megírva
* Érték: 2p
* Határidő: 2019. 10. 14.

## 03_HuffmanCodes

* Írj algoritmust, ami egy láncolt formában reprezentált (tavalyi félév: Node típus) Huffman-kódfa összes lehetséges ágát bejárja és egy OutStream objektumra a kapott kódolási megfeleltetéseket kiírja
* Minden ág kerüljön új sorba
* Minden sorba kerüljön az adott karakter Huffman-kódolásbeli kódja, majd egy szóközt követve maga az érintett karakter
* Minden esetben a bal ághoz rendeljük a 0-s, a jobbhoz az 1-es kódrészletet
* Határidő: 2019. 10. 14.

## 04_EfficientCompressionAlgorithm

* Adj meg úgy három stringet, hogy az egyik a Naiv, a másik a Huffman-, a harmadik az LZW-kódolásra adja a legjobb tömörítési arányt
* Alátámasztásul játszd végig mindhárom stringre mindhárom algoritmust
* A hatékonyságvizsgálatnál most ne számoljunk a Huffman esetén a fával, a másik kettő esetén az ábécével és azok méreteivel. Csak a kódolt outputot számoljuk, a lehető legkevesebb biten, amin megadható
* Határidő: 2019. 10. 14.

## 05_LzwWithCompressedDictionary

* Írj algoritmust, ami egy InStream formában megadott kódolandó stringet LZW módszerrel kódol, úgy, hogy az input mellett egy az ábécére inicializált stringtábla-referenciát is megkap, amibe az algoritmus futása végére a kódolás során előálló stringtábla tartalmát várjuk generálni
* A stringtábla értékeinél vagy definiáljunk egy adatszerkezetet, ami egy index és egy karakter párja; vagy vehetjük úgy, hogy egy string, aminek az utolsó karaktere a postfix karakter, az első "hossza-1" db pedig egy korábbi index a táblában
    * Részstringet vehetünk (igy: s = "abac", s<sub>2..3</sub> = "ba")
    * Van automatikus string-int konverziónk
* Határidő: 2019. 10. 14.

# AVL-fák

## 06_IsAvl

* Írj algoritmust, ami a paraméterként megadott láncolt reprezentációjú bináris fáról eldönti, hogy AVL-fa-e
* Tegyük fel, hogy a Node típus most nem tartalmazza a "b" adattagokat, ezeket tehát nem kell és nem is lehet használni
* Szülő pointer sincs
* Határidő: 2019. 10. 29.

## 07_convertAvl

* Írj algoritmust, ami a paraméterként megadott aritmetikai reprezentációjú (lásd előző félév) keresőfából láncolt reprezentációjú AVL-fát épít
* A tömb csak a kulcsokat tartalmazza, a felépített fa a tanult módon legyen ábrázolva, azaz left, right, key, b adattagokkal, értelemszerűen helyes értékekkel
* Az algoritmus legyen minél hatékonyabb
* Ha menet közben kiderül, hogy a fa nem AVL-fa (a keresőfa-tulajdonságot feltételezzük, ezt nem kell ellenőrizni), NULL-lal térjünk vissza (és ne is hagyjunk lógva egy általunk foglalt memóriaterületet sem)
* Határidő: 2019. 10. 29.

## 08_countBalanced

* Írj algoritmust, ami a hagyományos láncolt ábrázolással paraméterként megadott AVL-fáról megállapítja, hogy hány 0-s egyensúlyú csúcsa van
* Adj egy minél pontosabb alsó és felső becslést is a csúcsszám függvényében a kapott számra
* A pont akkor jár, ha az algoritmus helyes és hatékony, a becslés nem teljesen triviális, és meg van indokolva
* Határidő: 2019. 10. 29.

## 09_goodSeries

* Rögzített n>0-hoz megadható-e úgy egy n méretű AVL-fa és egy felső korlát egy kulcssorozat hosszára, hogy ezen sorozat minden elemét rendre a fába szúrva, annak magassága ne változzon?
* Miért igen/miért nem?
* Ha igen, adjunk egy egy ilyen korlátot n függvényében (tetszőleges n>0-ra)
* TFH, a kulcssorozat minden eleme egyedi, és egy eleme se szerepel a kiinduló fában
* Határidő: 2019. 10. 29.
